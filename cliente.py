from suma_pb2 import (
    Parametros                  # Clase que contiene como atributos de instancia, 
                                # los datos que contiene los parametros del servicio
)
from suma_pb2_grpc import (
    SumaNumerosEnterosStub      # Genera instancias de cliente que nos ayudaran 
                                # las cuales sirven para conectarse al servidor
)

import grpc

# Indicandole al cliente en que direccion ip y puerto se encuentra 
# ejecutandose el servidor
channel=grpc.insecure_channel("localhost:50051")

# Creando una instancia de cliente para comunicarnos con el servidor
cliente=SumaNumerosEnterosStub(channel)

# Creando el objeto con los valores de los parametros que 
# requiere el servicio
parametros_cliente=Parametros(numero_1=10,numero_2=20)

# Llamando al servidor a su funcion Sumar
respuesta=cliente.Sumar(parametros_cliente)

# Imprimiendo la respuesta
print(respuesta)



